from flask import Flask, abort, render_template, request, session, json, jsonify, Response, flash, redirect
import os
import boto.dynamodb2 as DynamoDB2
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
from boto.dynamodb2.fields import HashKey

sensors = Table('Sensors', connection = DynamoDB2.connect_to_region('eu-central-1'))

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(20);

@app.errorhandler(403)
def custom_403(error):
	return render_template('error.html', errornumber = 'Error 403', message = 'Acceso no autorizado.'), 403

@app.errorhandler(404)
def custom_404(error):
	return render_template('error.html', errornumber = 'Error 404', message = 'Nodo no existente.'), 404

@app.errorhandler(405)
def custom_405(error):
	return render_template('error.html', errornumber = 'Error 405', message = 'Nodo ya existente.'), 405

@app.route('/')
def index():
	return render_template('index.html')


@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/howto')
def howto():
	return render_template('howto.html')

@app.route('/exists/<sensorid>')
def api_exists(sensorid):
	if sensors.has_item(sensorid):
		return Response("Exists\n",200,{})
	else:
		return Response("Does not exist\n",404,{})

@app.route('/upload/<sensorid>', methods = ['POST'])
def api_upload(sensorid):
	try:
		sensor = sensors.get_item(Place=sensorid)
	except:
		return abort(405)
	else:
		req = request.get_json(force=True)
		if 'Public' in sensor:
			sensor['SensorData']=req['SensorData']
			sensor.partial_save()
		elif 'Password' in req and 'Password' in sensor and sensor['Password'] == req['Password']:
			sensor['SensorData']=req['SensorData']
			sensor.partial_save()
		else:
			return abort(405)
		return Response("200 OK - updated node\n",200,{})

@app.route('/retrieve/<sensorid>', methods = ['GET','POST'])
def api_retrieve(sensorid):
	print "hola"
	try:
		sensor = sensors.get_item(Place=sensorid)
	except:
		return Response('404 Not found', 404, {})
	else:
		if "Password" not in sensor:
			return json.dumps(sensor['SensorData']) + "\n"
		elif sensor['Password'] == request.get_json(force = True)['Password']:
			return Response(json.dumps(sensor['SensorData']) + "\n",200,{})
		else:
			return Response('403 Access forbidden\n', 403, {})

from flask_wtf import Form
from wtforms import StringField, PasswordField, SubmitField
class SensorForm(Form):
	place = StringField('Place')
	password = PasswordField('Password')
	submit = SubmitField('Send')

@app.route('/insert', methods = ['GET','POST'])
def insert():
	s = ""
	form = SensorForm()
	if request.method == 'POST':
		try:
			sensor = sensors.get_item(Place=form.place.data)
		except:
			data = {}
			data['Place'] = form.place.data
			data['SensorData'] = {}
			if form.password.data == "":
				data['Public'] = True
			else:
				data['Password'] = form.password.data
			sensors.put_item(data)
			s = render_template('success.html', name = form.place.data), 201
			pass
		else:
			s = abort(405)
	else:
		s = render_template('insert.html',form = form)
	return s

@app.route('/retrieve', methods = ['GET','POST'])
def retrieve():
	form = SensorForm()
	s = ""
	if request.method == 'POST':
		try:
			sensor = sensors.get_item(Place=form.place.data)
		except Exception, e:
			print e
			s = abort(404)
		else:
			data = {}
			if "Password" not in sensor or sensor['Password'] == form.password.data:
				s = render_template('data.html', place = form.place.data, data = sensor['SensorData'])
			else:
				s = abort(403)
	else:
		s = render_template('retrieve.html',form = form)
	return s

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)

