# README #

This is a personal project, but it has been made open in case others want to do a similar work (and also so my project director can access the live version!).

### What does this project do? ###

* Using an Amazon Web Services (AWS) EC2 instance as a database, to store any kind of information you want.
* Includes an (optional) access password for each entry in the database, which is required to store any data.
* This project is mostly intended for learning, so it may always be in a **beta** state!
* Also, each entry corresponds either to a real sensor node, or to dummy data for testing purposes.

### How does it work? ###

* Python 2.7.X, plus *boto*, *Flask* and *Flask-WTF* packages (easily found in pip).
* To run it on a machine, just do `python app.py` - with your own key (`.pem` file) in the same folder!

Method   | What it does
-------- | ------------
Insert   | Inserts (creates) a new entry in the database, with the specified name and password, if possible.
Upload   | Updates an existing entry in the database. Requires name, plus password and data in a JSON.
Retrieve | Returns the data from an existing entry in the database. Requires name and password.

Methods that start with `api_` are called from outside of the app (cURL, data sources...); the rest are internal.

### Who do I talk to? ###

* Repo owner or admin.
* More info in templates/about.html (or `52.28.90.29:5000/about`).